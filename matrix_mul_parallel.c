#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define SEED 123

void free_matrix(int **m, int size) {
    for (int i = 0; i < size; i++)
        free(m[i]);
    free(m);
}

int **mul(int **a, int **b, int size) {
    int **ret = malloc(size * sizeof(int *));
    for (int i = 0; i < size; i++) {
        ret[i] = calloc(size, sizeof(int));
        for (int j = 0; j < size; j++)
            for (int k = 0; k < size; k++)
                ret[i][j] += a[i][k] * b[k][j];
    }

    free_matrix(a, size);
    free_matrix(b, size);

    return ret;
}

// Parallelise this function:
int **array_mul(int ***data, int n, int size) {
    int nt = omp_get_num_procs();
    while(n>1){
    #pragma omp taskloop num_tasks(nt) shared(n,data,nt)
        for (int i = 0; i < n -1; i+=2)
            data[i/2] = mul(data[i], data[i+1], size);
    #pragma  omp taskwait
        n = n/2;
    }

    return data[0];
}

int **rnd_matrix(int size) {
    int **ret = malloc(size * sizeof(int *));
    for (int i = 0; i < size; i++) {
        ret[i] = malloc(size * sizeof(int));
        for (int j = 0; j < size; j++)
            ret[i][j] = 2 * (rand() % 2) - 1; // Generates -1 or 1
    }

    return ret;
}

void print_matrix(int **m, int size) {
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++)
            printf("%d ", m[i][j]);
        printf("\n");
    }
}

int main() {
    int n, size;
    scanf("%d %d", &n, &size);

    srand(SEED);

    int ***data = malloc(n * sizeof(int **));
    for (int i = 0; i < n; i++)
        data[i] = rnd_matrix(size);

    int **ret = array_mul(data, n, size);
    print_matrix(ret, size);

    free_matrix(ret, size);
    free(data);
    return 0;
}
